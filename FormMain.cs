namespace McOrders
{
    public partial class FormMain : Form
    {
        private Priprema priprema;
        public FormMain()
        {
            InitializeComponent();
            priprema = new Priprema();
            priprema.KreniEvent += Priprema_Osvjezi;
            priprema.GotovoEvent += Priprema_Osvjezi;
        }

        private void Priprema_Osvjezi(object sender, GotovoArgs e)
        {
            if (InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate
                {
                    osvjeziPrikaz();
                }));
                return;
            }
            else osvjeziPrikaz();
        }

        private void osvjeziPrikaz()
        {
            lbActiveOrders.Items.Clear();
            lbCompletedOrders.Items.Clear();
            lbActiveOrders.Items.AddRange(priprema.Red.ToArray());
            lbCompletedOrders.Items.AddRange(priprema.Gotovo.ToArray());
        }

        private void btnPommes_Click(object sender, EventArgs e)
        {
            priprema.naruciPomfri();
        }

        private void btnBigMac_Click(object sender, EventArgs e)
        {
            priprema.naruciBurger();
        }

        private void btnDrink_Click(object sender, EventArgs e)
        {
            priprema.naruciPice();
        }
    }
}