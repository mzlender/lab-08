﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.TaskbarClock;


namespace McOrders
{
    public class Priprema
    {
        public int RedniBroj = 0;
        public List<Narudzba> Red = new List<Narudzba>();
        public List<Narudzba> Gotovo = new List<Narudzba>();
        public List<Task> Tasks = new List<Task>();
        public void naruciPice() {
            RedniBroj++;
            Red.Add(new Pice(RedniBroj));
            KreniEvent?.Invoke(this, new GotovoArgs());
            Task piceGotovo = new Task(() =>
            {
                Thread.Sleep(4000);
                gotovoPice();
            });
            piceGotovo.Start();
        }
        public void naruciPomfri()
        {
            RedniBroj++;
            Red.Add(new Pomfri(RedniBroj));
            KreniEvent?.Invoke(this, new GotovoArgs());
            Task pomfriGotovo = new Task(() =>
            {
                Thread.Sleep(7000);
                gotovoPomfri();
            });
            pomfriGotovo.Start();
        }
        public void naruciBurger()
        {
            RedniBroj++;
            Red.Add(new Burger(RedniBroj));
            KreniEvent?.Invoke(this, new GotovoArgs());
            Task burgerGotovo = new Task(() =>
            {
                Thread.Sleep(12000);
                gotovoBurger();
            });
            burgerGotovo.Start();
        }
        public void gotovoPice()
        {
            foreach (var narudzba in Red) {
                if (narudzba is Pice)
                {
                    Gotovo.Add(narudzba);
                    Red.Remove(narudzba);
                    GotovoEvent?.Invoke(this, new GotovoArgs());
                    break;
                }
            }
        }
        public void gotovoPomfri()
        {
            foreach (var narudzba in Red)
            {
                if (narudzba is Pomfri)
                {
                    Gotovo.Add(narudzba);
                    Red.Remove(narudzba);
                    GotovoEvent?.Invoke(this, new GotovoArgs());
                    break;
                }
            }
        }
        public void gotovoBurger()
        {
            foreach (var narudzba in Red)
            {
                if (narudzba is Burger)
                {
                    Gotovo.Add(narudzba);
                    Red.Remove(narudzba);
                    GotovoEvent?.Invoke(this, new GotovoArgs());
                    break;
                }
            }
        }



        public delegate void GotovoDelegate(object sender, GotovoArgs e);
        public event GotovoDelegate GotovoEvent;
        public event GotovoDelegate KreniEvent;
    }
}
