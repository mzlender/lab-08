﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace McOrders
{
    public class Narudzba
    {
        public int RedniBroj { get; set; }
        public string Ime { get; set; }
        public int duration { get; set; }

        public Narudzba(int RedniBroj) 
        {
            this.RedniBroj = RedniBroj;
        }
        public Narudzba() { }

        public override string ToString()
        {
            return RedniBroj.ToString() + " " + Ime;
        }
    }
}
